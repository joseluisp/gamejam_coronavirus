﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using System;


public class CinematicaPrincipio : MonoBehaviour
{

    [SerializeField] private SpriteRenderer fondo = null;
    [SerializeField] private SpriteRenderer player = null;
    [SerializeField] private GameObject fundidoNegro = null;
    [SerializeField] private GameManager gameManager = null;
    [SerializeField] public Animation animation = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public async void Fundido()
    { 
        
        fondo.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        player.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        fundidoNegro.SetActive(true);

        animation.Play("cinematica_fundidonegro");
        await UniTask.Delay(TimeSpan.FromSeconds(2));
        


    }

    public void InitPartida()
    { 
        
        gameManager.InitPartida();
    
    }

}
