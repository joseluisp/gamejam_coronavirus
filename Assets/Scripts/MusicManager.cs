﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UniRx.Async;
using System;


public class MusicManager : MonoBehaviour
{

    [SerializeField] private AudioMixer mixer = null;

    [Header("Musica")]
    [SerializeField] private AudioSource musicSource = null;
    [SerializeField] private AudioSource musicSource2 = null;
    [SerializeField] private AudioSource musicSinEfecto = null;


    [Header("efectos")]
    [SerializeField] private AudioSource sfxAterrizaje = null;
    [SerializeField] private AudioSource sfxDash = null;
    [SerializeField] private AudioSource sfxJump = null;
    [SerializeField] private AudioSource sfxPasos = null;
    [SerializeField] private AudioSource sfxGotas = null;
    [SerializeField] private AudioSource sfxsinEfecto = null;

    
    
   
    [Header("SFX")]
    public AudioClip[] sfx;

    [Header("Sounds")]
    public AudioClip[] sounds;

    public static MusicManager MusicInstance { get; private set; } = null;

    private bool isMuted = false;

    void Awake()
    {

        if (MusicInstance != null && MusicInstance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            MusicInstance = this;
        }
        //DontDestroyOnLoad(this.gameObject);


    }


    public void PlayWithoutEfects(AudioClip clip, bool loop)
    { 
        musicSinEfecto.Stop();
        musicSinEfecto.loop = loop;
        musicSinEfecto.clip = clip;
        musicSinEfecto.Play();
    
    }

    public void PlayFXWhioutEffects(AudioClip clip)
    { 
        
        sfxsinEfecto.Stop();
        sfxsinEfecto.clip = clip;
        sfxsinEfecto.PlayOneShot(clip);
    
    }

    public void StopWhioutEffects()
    { 
    
        sfxsinEfecto.Stop();
    
    }

     

    public void PlayInGameMusic(AudioClip clip, bool loop, bool effect)
    {
        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }
        
        if (effect == false)
        { 
            PlayWithoutEfects(clip, loop);
            return;
        }
        
        
        musicSource.Stop();
        musicSource.loop = loop;
        musicSource.clip = clip;
        musicSource.Play();

    }


    public async void StopInGameMusic()
    { 
    

        float currentSoundsVolumen = -1f;
        mixer.GetFloat("soundsVolumen", out currentSoundsVolumen);

        StartCoroutine(FadeMixerGroup.StartFade(mixer, "soundsVolumen", 1f, -80f));
        StartCoroutine(FadeMixerGroup.StartFade(mixer, "sounds_menu", 1f, -80f));
        await UniTask.Delay(TimeSpan.FromSeconds(1));

        musicSource.Stop();

        mixer.SetFloat("soundsVolumen", currentSoundsVolumen);
        mixer.SetFloat("sounds_menu", currentSoundsVolumen);

    }

    public void PlayInGameMusic2(AudioClip clip, bool loop = true, bool effect = false)
    {
        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayWithoutEfects(clip, loop);
            return;
        }

        musicSource2.Stop();
        musicSource.bypassEffects = effect;
        musicSource.bypassReverbZones = effect;


        musicSource2.loop = loop;
        musicSource2.clip = clip;
        musicSource2.Play();

    }

    
    public void StopPlayInGameMusic2()
    { 
    
        musicSource2.Stop();
    
    }



    public void PlayFXMenu(AudioClip clip, bool effect)
    {

        //if (sfxSource.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

        

        sfxAterrizaje.bypassEffects = effect;
        sfxAterrizaje.bypassReverbZones = effect;

        sfxAterrizaje.clip = clip;
        sfxAterrizaje.PlayOneShot(sfxAterrizaje.clip);



    }

    //public void PlayFXSound2(AudioClip clip)
    //{

    //    if (sfxDash.isPlaying == true) return;

    //    if (clip == null)
    //    {
    //        Debug.LogError("Audio clip no existe");
    //        return;

    //    }

    //    sfxDash.clip = clip;
    //    sfxDash.Play();



    //}

    public void PlayFXGotas(AudioClip clip, bool effect)
    {

        if (sfxGotas.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

        sfxGotas.bypassEffects = effect;
        sfxGotas.bypassReverbZones = effect;

        sfxGotas.clip = clip;
        sfxGotas.Play();



    }


    public void PlayFXPasos(AudioClip clip, bool loop = false, bool effect = false)
    {

        if (sfxPasos.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

         sfxPasos.bypassEffects = effect;
        sfxPasos.bypassReverbZones = effect;
        
        sfxPasos.loop = loop;
        sfxPasos.clip = clip;
        sfxPasos.Play();



    }


    public void StopFXPasos()
    { 
    
        sfxPasos.Stop();
    }

    public void PlayFXJump(AudioClip clip, bool effect)
    {

        if (sfxJump.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

         sfxJump.bypassEffects = effect;
        sfxJump.bypassReverbZones = effect;

        sfxJump.clip = clip;
        sfxJump.Play();



    }


    public void PlayFXAterrizaje(AudioClip clip, bool effect)
    {

        //if (sfxAterrizaje.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

         sfxAterrizaje.bypassEffects = effect;
        sfxAterrizaje.bypassReverbZones = effect;

        sfxAterrizaje.clip = clip;
        sfxAterrizaje.Play();



    }


    public void PlayFXDash(AudioClip clip, bool effect )
    {

        //if (sfxDash.isPlaying == true) return;

        if (clip == null)
        {
            Debug.LogError("Audio clip no existe");
            return;

        }

        if (effect == false)
        { 
            PlayFXWhioutEffects(clip);
            return;
        }

         sfxDash.bypassEffects = effect;
        sfxDash.bypassReverbZones = effect;

        sfxDash.clip = clip;
        sfxDash.Play();



    }




    public void SettingsMixer(bool isMuted, ushort volumenMain, ushort volumenMusic, ushort volumenSfx)
    {


        if (isMuted == true)
        {

            mixer.SetFloat("masterVolumen", -80f);

            return;

        }

        if (volumenMusic == 10)
        {

            mixer.SetFloat("soundsVolumen", -30f);
            return;
        }

        if (volumenMusic == 0)
        {

            mixer.SetFloat("soundsVolumen", -80f);
            return;
        }

        if (volumenSfx == 0)
        {

            mixer.SetFloat("fxVolumen", -80f);
            return;
        }

        if (volumenSfx == 10)
        {

            mixer.SetFloat("fxVolumen", -30f);
            return;
        }



        mixer.SetFloat("masterVolumen", Mathf.Log10((  volumenMain - 10) * 0.01f) * 20);
        mixer.SetFloat("soundsVolumen", Mathf.Log10((volumenMusic - 10) * 0.01f) * 20 );
        mixer.SetFloat("fxVolumen", Mathf.Log10((volumenSfx - 10) * 0.01f) * 20 );



    }


    public void Mute()
    {

        isMuted = !isMuted;

        musicSource.mute = isMuted;
        sfxAterrizaje.mute = isMuted;
       
    }

    public void MuteOff()
    {
        isMuted = false;
        musicSource.mute = false;
        sfxAterrizaje.mute = false;
    }

    public void SetVolumenMaster(ushort volumenMain)
    {


        if (volumenMain == 0)
        {
            
            mixer.SetFloat("masterVolumen", -80f);
            return;
        }

        if (volumenMain == 10)
        {

            mixer.SetFloat("masterVolumen", -30f);
            return;
        }

        mixer.SetFloat("masterVolumen", Mathf.Log10((volumenMain - 10) * 0.01f) * 20);

    }

    public void SetVolumenSounds(ushort volumenMusic)
    {



        if (volumenMusic == 0)
        {

            mixer.SetFloat("soundsVolumen", -80f);
            return;
        }


        if (volumenMusic == 10)
        {

            mixer.SetFloat("soundsVolumen", -30f);
            return;
        }

        mixer.SetFloat("soundsVolumen", Mathf.Log10((volumenMusic - 10) * 0.01f) * 20);

    }

    public void SetVolumenSfx(ushort volumenSfx)
    {


        if (volumenSfx == 0)
        {

            mixer.SetFloat("fxVolumen", -80f);
            return;
        }

        if (volumenSfx == 10)
        {

            mixer.SetFloat("fxVolumen", -30f);
            return;
        }

        mixer.SetFloat("fxVolumen", Mathf.Log10((volumenSfx - 10) * 0.01f) * 20);

    }



}
