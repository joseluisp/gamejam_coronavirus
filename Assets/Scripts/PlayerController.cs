﻿using UnityEngine;
using UnityEngine.Events;
using UniRx.Async;
using System;
using TMPro;


public class PlayerController : MonoBehaviour
{
    [Header("Texto")]
    [SerializeField] public TextMeshProUGUI txtTexto = null;


	[SerializeField] private float m_JumpForce = 400f;
	[Range(0, 0.3f)] [SerializeField] private float m_MovementSmoothing = 0.05f;	
	[SerializeField] private bool m_AirControl = false;							
	[SerializeField] private LayerMask m_WhatIsGround;
    [SerializeField] private LayerMask m_WhatIsEscalera;
	[SerializeField] private Transform m_GroundCheck;							
	[SerializeField] private float dashSpeed;
	[SerializeField] private TrailRenderer trail = null;
	[SerializeField] private float cameraShakeDash = 0.1f;
	[SerializeField] private float cameraShakeJump = 0.1f;
	[SerializeField] private Vector2 wallHopDirection = new Vector2(1, 0.5f); 
	[SerializeField] private Vector2 wallJumpDirection = new Vector2(1, 2f);
    [SerializeField] private float gravityScale = 4;
    [SerializeField] private bool isFake = false;

    private PlayerMovement PlayerMov;
	private const float k_GroundedRadius = 0.3f; 

	public float wallHopForce;
    public float wallJumpForce;
    public bool DirectionWallSlide;
    public bool CanMove = true;
	public bool isGrounded = false;            
	private bool m_FacingRight = true;
	public bool isLanding = false; 
	public bool isTouchingWall = false;
	private bool isFlipedinWall = false;
    private bool isSliding = false;

	private Rigidbody2D m_Rigidbody2D;
	
	private Vector3 m_Velocity = Vector3.zero;

    ///Dash Variables////
    ///***************///
    public float DashTime;
    float TimingDashing;
    public float DashSpeed;
    bool dashing = false;
	//----
	private bool isWallSliding;
	private bool isAttemptingToJump;
	private bool checkJumpMultiplier;
	private bool hasWallJumped;
	private bool canMove;
	private bool canFlip;
	private int amountOfJumpsLeft;
	private int lastWallJumpDirection;
	private int facingDirection = 1;

	public float wallJumpTimerSet = 0.5f;

	private float wallJumpTimer;
	public float turnTimerSet = 0.1f;
	private float jumpTimer;

	public int amountOfJumps = 1;
	private float turnTimer;
	


	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	private void Awake()
	{
		m_Rigidbody2D = this.GetComponent<Rigidbody2D>();

        m_Rigidbody2D.gravityScale = gravityScale;

		wallHopDirection.Normalize();
		wallJumpDirection.Normalize();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

	}
    private void Start()
    {
        PlayerMov = GetComponent<PlayerMovement>();
    }
    private void FixedUpdate()
	{

        if (isFake) return;

		bool wasGrounded = isGrounded;
		isGrounded = false;
        PlayerMov.IsGround = false;
        //isLanding = Physics2D.OverlapCircle(m_GroundCheck.position, 0.15f, m_WhatIsGround);
        //RaycastHit2D hit = Physics2D.Raycast(this.transform.position, Vector2.up, m_WhatIsEscalera);
        //if (hit.collider != null)
        //{ 
        //    PlayerMov.Set_ClimbStairs();
        
        //}
        //else
        //{ 
        //    PlayerMov.Set_ExitStairs();
        
        //}

        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
           

            
			if (colliders[i].CompareTag("Wall"))
			{
                //print(m_Rigidbody2D.velocity.y);
                //PlayerMov.Sliding = true;
                //print(colliders[i].transform.position.ToString());
                //print(transform.position.ToString());
                if(colliders[i].transform.position.x > transform.position.x)
                {
                    if (Input.GetAxisRaw("Horizontal") > 0)
                    {

                        if (m_Rigidbody2D.velocity.y < 0)
                        {
                            PlayerMov.Sliding = true;
                            isTouchingWall = true;
                            m_Rigidbody2D.drag = 40f;
                            PlayerMov.Jumping = false;
                            //print("Entro");
                        }
                    }
                    else
                    {
                        PlayerMov.Sliding = false;
                    }
                }
                else
                {
                    if (Input.GetAxisRaw("Horizontal") <  0)
                    {
                        if (m_Rigidbody2D.velocity.y < 0)
                        {
                            PlayerMov.Sliding = true;
                            isTouchingWall = true;
                            m_Rigidbody2D.drag = 40f;
                            PlayerMov.Jumping = false;
                            //print("Entro");
                        }

                    }
                    else
                    {
                        PlayerMov.Sliding = false;
                    }
                }
                
			}
            else
            {
                PlayerMov.Sliding = false;
                if (colliders[i].CompareTag("Ground") || colliders[i].CompareTag("Objetos"))
                {


                    



                    var rnd = UnityEngine.Random.Range(10, 13);
                    MusicManager.MusicInstance.PlayFXAterrizaje(  MusicManager.MusicInstance.sfx[rnd], true );


                    isGrounded = true;
                    isFlipedinWall = false;
                    isTouchingWall = false;
                    PlayerMov.Jumping = false;
                    PlayerMov.IsGround = true;
                    if (!wasGrounded)
                    {
                        CameraShake.currentShake += cameraShakeJump;
                        m_Rigidbody2D.drag = 0;
                        OnLandEvent.Invoke();
                    }

                }
            }

			
            if (colliders[i].CompareTag("SiguienteFase"))
            { 
                
                txtTexto.text = "Aislado!";

            }


		}


	}

	 


	private void WallJump(float moveX)
    {
		//print(moveX);
		if (moveX == 0)
		{ 
			//moveX = 2.5f;
		}
		if (m_FacingRight == true)
		{ 
			moveX = -2f;
            Flip();
		}
        else
        {
            moveX = 2f;
            Flip();
        }
		//isTouchingWall = false;
		//isFlipedinWall = false;
		m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0f);
		isWallSliding = false;
        amountOfJumpsLeft = amountOfJumps;
        amountOfJumpsLeft--;
		m_Rigidbody2D.drag = 0f;
        Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * moveX, wallJumpForce * wallJumpDirection.y);
		//print(forceToAdd);
        m_Rigidbody2D.AddForce(forceToAdd, ForceMode2D.Impulse);
		


		//jumpTimer = 0;
		//isAttemptingToJump = false;
		//checkJumpMultiplier = true;
		//turnTimer = 0;
		//canMove = true;
		//canFlip = true;
		//hasWallJumped = true;
		//wallJumpTimer = wallJumpTimerSet;
		//lastWallJumpDirection = -facingDirection;

	}



	private void OnCollisionExit2D(Collision2D collision)
	{

		m_Rigidbody2D.drag = 0f;
		if (collision.gameObject.CompareTag("Wall"))
		{ 
			isFlipedinWall = false;
			isTouchingWall = false;
            PlayerMov.Sliding = false;

        }
	}


    public async void Move(float moveX, float moveY, bool normalJump, bool dash, bool canWallJump, float JumpVel, bool isUppingStairs)
    {
        if (m_Rigidbody2D == null ) return;

        if (dashing)
        {
            trail.emitting = true;
            float direction = 1f;
            if (m_FacingRight == false)
            {
                direction = -1f;
            }
            //************Quite esta parte para probar un dash diferente******************//
            //Vector3 targetVelocity = new Vector2(direction * dashSpeed, m_Rigidbody2D.velocity.y);
            //var force = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            //m_Rigidbody2D.AddForce(force);
            //////////////////////////////////////////////////////////////////////
            if (TimingDashing <= DashTime)
            {
                TimingDashing += Time.deltaTime;

                Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
                for (ushort i = 0; i < colliders.Length; i++)
                { 
                    if (colliders[i].CompareTag("Rendijas"))
                    { 
                        colliders[i].GetComponent<BoxCollider2D>().enabled = false;

                        switch (direction)
                        {
                            case -1:
                                m_Rigidbody2D.velocity = Vector2.left * dashSpeed;
                                break;
                            case 1:
                                m_Rigidbody2D.velocity = Vector2.right * dashSpeed;
                                break;
                        }

                        CanMove = false;
                        normalJump = false;
                        await UniTask.Delay(TimeSpan.FromMilliseconds(200));
                        colliders[i].GetComponent<BoxCollider2D>().enabled = true;
                        return;
                    }
                
                
                }


                switch (direction)
                {
                    case -1:
                        m_Rigidbody2D.velocity = Vector2.left * dashSpeed;
                        break;
                    case 1:
                        m_Rigidbody2D.velocity = Vector2.right * dashSpeed;
                        break;
                }
                

                CanMove = false;
                normalJump = false;
                print(TimingDashing.ToString());
            }
            else
            {

                Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
                for (ushort i = 0; i < colliders.Length; i++)
                { 
                    if (colliders[i].CompareTag("Rendijas"))
                    { 
                        switch (direction)
                        {
                            case -1:
                                m_Rigidbody2D.velocity = Vector2.left * dashSpeed;
                                break;
                            case 1:
                                m_Rigidbody2D.velocity = Vector2.right * dashSpeed;
                                break;
                        }

                        break;
                    }
                
                
                }
                
                dashing = false;
                TimingDashing = 0;
                CanMove = true;
                m_Rigidbody2D.velocity = Vector2.zero;
                PlayerMov.SetNotDash();
                //await UniTask.Delay(TimeSpan.FromSeconds(trail.time));
                trail.emitting = false;



            }
        }
        if (isGrounded || m_AirControl && !isTouchingWall)
        {
            if (dash == true)
            {
                if (!dashing)
                {
                    print("dash!");
                    CameraShake.currentShake += cameraShakeDash;
                    dashing = true;
                }
                
                //row--
                //m_Rigidbody2D.AddForce(new Vector2(10f * dashSpeed, m_Rigidbody2D.velocity.y));


            }
            else
            {
                if (CanMove)
                {
                    

                    m_Rigidbody2D.drag = 0f;
                    Vector3 targetVelocity = new Vector2(moveX * 10f, m_Rigidbody2D.velocity.y);
                    m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

                    if (moveX > 0 && !m_FacingRight)
                    {
                        Flip();
                    }
                    else if (moveX < 0 && m_FacingRight)
                    {
                        Flip();
                    }
                }
            }


        }
        if (isGrounded == false && canWallJump == true && CanMove)
        {
            WallJump(moveX);
            CanMove = false;
            Invoke("ReturCanMove", 0.2f);
            return;

        }

        if (isUppingStairs == true)
        { 

            Vector3 targetVelocity = new Vector2(m_Rigidbody2D.velocity.x, moveY * 10f);
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            m_Rigidbody2D.gravityScale = 0;
        }

        if (normalJump && CanMove)
        {
            isGrounded = false;
            m_Rigidbody2D.velocity = Vector2.up * JumpVel;
            m_Rigidbody2D.velocity = new Vector2(moveX * 10, m_Rigidbody2D.velocity.y);
        }
    }


	private void Flip()
	{
		m_FacingRight = !m_FacingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		
		

	}

    private void ReturCanMove()
    {
        CanMove = true;
    }

    public void Reset_Gravity()
    { 
        m_Rigidbody2D.gravityScale = gravityScale;
    
    }




}