﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using System;

public class GotasController : MonoBehaviour
{
    
    [SerializeField] private bool isUsing = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }


    private async void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (isUsing == true) return;

        isUsing = true;
        var rnd = UnityEngine.Random.Range(2, 5);
        MusicManager.MusicInstance.PlayFXGotas( MusicManager.MusicInstance.sfx[rnd], true);

        await UniTask.Delay(TimeSpan.FromSeconds(MusicManager.MusicInstance.sfx[rnd].length));
        isUsing = false;


    }

}
