﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private PlayerAnimationsController animationsController = null;
    [SerializeField] private PlayerController playerController = null;
    [SerializeField] private float runSpeed = 40f;
    [SerializeField] private float verticalSpeed = 10f;
    [SerializeField] private ParticleSystem particula = null;
    [SerializeField] private KeyCode teclaActivar = KeyCode.Q;
    [SerializeField] private KeyCode teclaDesactivar = KeyCode.E;

    private float horizontalMove = 0f;
    private float verticalMove = 0f;
    private bool jumpNormal = false;
    private bool canWallJump = false;
    private bool attack = false;
    private bool dash = false;
    private bool canUpStairs = false;
    private bool isUppingStairs = false;


    public bool Jumping = false;
    public bool IsGround = false;
    //Jump
    private float JumpVelocity = 10f;
    public float MaxTimePressJump = 0.5f;
    private float JumpTimer = 0f;
    public bool IsPressinJump = false;
    //
    public bool Sliding = false;
    

    private bool entradoActivador = false;
    public bool realizarActivacion = false;
    public bool realizarDesActivacion = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void ShowParticlesMove()
    { 
        animationsController.Set_Move();
        var tempRot = particula.transform.rotation;
        tempRot.eulerAngles = new Vector3(0, 0, 135f);
        particula.transform.rotation = tempRot;
        particula.Play();
    
    }

    public void SetJumping()
    { 
        jumpNormal = true;
        Jumping = true;
        animationsController.Set_Jump();
        IsPressinJump = true;
    
    }

    // Update is called once per frame
    void Update()
    { 

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        verticalMove = Input.GetAxisRaw("Vertical") * verticalSpeed;  

        //print(horizontalMove);
        if (horizontalMove != 0)
        { 

            if (particula.isPlaying == false)
            { 

                if (horizontalMove > 0)
                { 
                    var tempRot = particula.transform.rotation;
                    tempRot.eulerAngles = new Vector3(0, 0, 135f);
                    particula.transform.rotation = tempRot;

                    
                }
                else if (horizontalMove < 0)
                { 
                    var tempRot = particula.transform.rotation;
                    tempRot.eulerAngles = new Vector3(0, 0, 0);
                    particula.transform.rotation = tempRot;
                    
                    
                }
                    
                particula.Play();
            }
            if(IsGround)
            {
                 MusicManager.MusicInstance.PlayFXPasos( MusicManager.MusicInstance.sfx[5], true);
                animationsController.Set_Move();
            }
        }
        else
        {
            if (IsGround)
            {
                animationsController.Set_Idle();
            }
        }
        if(Input.GetButtonDown("Jump"))
        {
            // atascado aqui
            //print(" puedo subir ??? .... " + canUpStairs);

            var rnd = UnityEngine.Random.Range(6, 9);
            MusicManager.MusicInstance.PlayFXJump(  MusicManager.MusicInstance.sfx[rnd], true );

            if (canUpStairs == true)
            { 
                //print("subiendo .... ");
                isUppingStairs = true;
                return;
            }

            if (!Jumping)
            {
                //print("touchin wall=" + playerController.isTouchingWall + " ground=" + playerController.isGrounded);
                if (playerController.isTouchingWall == true && playerController.isGrounded == false)
                {

                    canWallJump = true;
                    Invoke("DontWallJumping", 0.3f);
                    Jumping = true;
                    animationsController.SetWallJump();
                    
                }
                else
                {
                    
                    if(IsGround)
                    {
                        jumpNormal = true;
                        Jumping = true;
                        animationsController.Set_Jump();
                        IsPressinJump = true;
                        Invoke("ToFalling", 0.4f);
                    }
                }
            }
            
        }

        if (Input.GetButtonUp("Jump"))
        {
            IsPressinJump = false;
            jumpNormal = false;
            JumpTimer = 0;
        }

        if (Input.GetButton("Jump"))
        {
            JumpTimer += Time.deltaTime;
            if (JumpTimer >= MaxTimePressJump)
            {
                jumpNormal = false;
                IsPressinJump = false;
                //print(JumpTimer.ToString());
            }
            
        }

        if(Input.GetKeyDown(teclaActivar))
        { 
            if (entradoActivador == true)
            { 
                realizarActivacion = true;
                realizarDesActivacion = false;
            }
            
        }

        if(Input.GetKeyDown(teclaDesactivar))
        { 

            if (entradoActivador == true)
            { 
                realizarDesActivacion = true;
                realizarActivacion = false;
            }
        
        }



        if(Input.GetButtonDown("Dash"))
        { 
            MusicManager.MusicInstance.PlayFXDash(  MusicManager.MusicInstance.sfx[9], true );
            //print("dash");
            dash = true;
            animationsController.Set_Dash();        
        
        }
        if (Sliding)
        {
            animationsController.Set_Slide();
        }
        else
        {
            animationsController.SetNotSliding();
        }
        
    }

    private void FixedUpdate()
    {

        playerController.Move(horizontalMove * Time.fixedDeltaTime, verticalMove * Time.fixedDeltaTime, jumpNormal, dash, canWallJump,JumpVelocity, isUppingStairs);
        
        //jumpNormal = false;
        canWallJump = false;
        dash = false;


    }
    private void ToFalling()
    {
        
        animationsController.SetFalling();
        //print("Falling");
    }
    private void DontWallJumping()
    {
        animationsController.SetNotWallJump();
    }


    public void EntradaActivador()
	{ 
		
		entradoActivador = true;
	
	}

	public void SalirActivador()
	{ 
		
		entradoActivador = false;
        realizarActivacion = false;
        realizarDesActivacion = false;
        

	}


    public void Set_ClimbStairs()
    { 
        canUpStairs = true;
        //isUppingStairs = false;
    
    }

    public void Set_ExitStairs()
    { 
        canUpStairs = false;
        isUppingStairs = false;
        playerController.Reset_Gravity();
    
    
    }
    public void SetNotDash()
    {
        animationsController.SetNot_Dash();
    }


}
