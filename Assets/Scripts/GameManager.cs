﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Async;
using System;
using TMPro;

public class GameManager : MonoBehaviour
{

    [Header("Managers")]
    [SerializeField] private MenuManager menuManager = null;

    [SerializeField] private bool empezado = false;
    [Header("Musica Refuerzo")]
    [SerializeField] private bool onMusicaRefuerzo = false;
    [SerializeField] private ushort segundosMusicaRefuerzo = 41;

    [Header("Gotas")]
    [SerializeField] private bool onGotas = false;
    [SerializeField] private ushort segundosGotas = 15;


    

    IDisposable gotasRandom = null;
    IDisposable musicRandom = null;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    public async void InitPartida()
    { 


        menuManager.reverberacion.enabled = true;
        menuManager.canvasMenu[10].SetActive(true);

        menuManager.canvasMenu[5].SetActive(true);
        menuManager.canvasMenu[3].SetActive(true);
        menuManager.canvasMenu[7].SetActive(true);
        menuManager.canvasMenu[6].SetActive(false);

        menuManager.canvasMenu[8].SetActive(false);
        var tAnimacion = menuManager.canvasMenu[10].GetComponent<Animation>();
        tAnimacion.Play("cinematica_fundidoblanco");

        await UniTask.Delay(TimeSpan.FromSeconds(tAnimacion.GetClip("cinematica_fundidoblanco").length));


        empezado = true;
        SettingsSFXAndMusic();

        

    
    }


    private void SettingsSFXAndMusic()
    { 
    
        MusicManager.MusicInstance.PlayInGameMusic( MusicManager.MusicInstance.sounds[1], true, true);

        if (onGotas == true)
        { 
            
            //cada 10 segundos sonara un sonidos FX de gota caer
            gotasRandom = Observable.Timer(
            TimeSpan.FromSeconds(10), //esperamos 1 segundos 
            TimeSpan.FromSeconds(segundosGotas), Scheduler.MainThread).Do(x => { }).
            ObserveOnMainThread()
            .Subscribe
            (_ =>
            {
                var rnd = UnityEngine.Random.Range(2, 5);
                MusicManager.MusicInstance.PlayFXGotas( MusicManager.MusicInstance.sfx[rnd], true  );


            }
            , ex => { Debug.Log(" sonidosRandom OnError:" + ex.Message); if (gotasRandom != null) gotasRandom.Dispose(); },
            () => //completado
            {
                if (gotasRandom != null)
                { 
                    gotasRandom.Dispose();
            
                }

            }).AddTo(this.gameObject);
        
        
        }

    
        if (onMusicaRefuerzo == true)
        { 
            musicRandom = Observable.Timer(
            TimeSpan.FromSeconds(14), //esperamos 1 segundos 
            TimeSpan.FromSeconds(segundosMusicaRefuerzo), Scheduler.MainThread).Do(x => { }).
            ObserveOnMainThread()
            .Subscribe
            (_ =>
            {
            
                var rnd = UnityEngine.Random.Range(2, 5);
                MusicManager.MusicInstance.PlayInGameMusic2( MusicManager.MusicInstance.sounds[rnd], true  );


            }
            , ex => { Debug.Log(" sonidosRandom OnError:" + ex.Message); if (musicRandom != null) musicRandom.Dispose(); },
            () => //completado
            {
                if (musicRandom != null)
                { 
                    musicRandom.Dispose();
            
                }

            }).AddTo(this.gameObject);
        
        }

        
    
    
    
    }


    //public void SonarMusicaRefuerzo()
    //{ 
    //    var rnd = UnityEngine.Random.Range(2, 5);
    //    MusicManager.MusicInstance.PlayInGameMusic2(
    //        MusicManager.MusicInstance.sounds[rnd], false
    //    );
    
    
    //}


}
