﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UniRx.Async;
using IgniteModule.TypeWrite;

public class MenuManager : MonoBehaviour
{

    [Header("Game MAnager")]
    [SerializeField] private GameManager gameManager = null;


    [SerializeField] public GameObject[] canvasMenu = null;

    [Header("Menu Principal")]
    [SerializeField] private TextMeshProUGUI[] textos = null;
    [SerializeField] private Color notSelectedColor = Color.white;
    [SerializeField] private Color selectedColor = Color.white;
    [SerializeField] private RectTransform punteroMenuPrincipal = null;
    [SerializeField] private GameObject camera = null;
    

    [SerializeField] private RectTransform[] positionpunteroMenuPrincipal = null;


    [SerializeField] private ParticleSystem particleOptionsSelected = null;
    [SerializeField] private RectTransform particleTransformOptions = null;
    [SerializeField] private CanvasGroup canvasAlphaMenu = null;
    [SerializeField] private Animation animations = null;
    [SerializeField] private Animation cinematicAnimation = null;


    private bool isMutedDefault = false;
    private ushort mainVolumenDefault = 100;
    private ushort mainSoundDefault = 60;
    private ushort mainSfxDefault = 20;

    private bool isMutedInternal = false;
    private ushort mainVolumenInternal = 100;
    private ushort mainSoundInternal = 60;
    private ushort mainSfxInternal = 20;


    private bool isBegun = false;
    
    

    [Header("Options Menu")]
    [SerializeField] private TextMeshProUGUI[] textosOptions = null;
    [SerializeField] private TextMeshProUGUI[] txtOptions = null;
    [SerializeField] private RectTransform punteroMenuOpciones = null;

    [Header("opciones debug")]
    [SerializeField] private bool debug = true;

    [Header("Dialogos")]
    [SerializeField] private Image imagenBocadillo = null;
    [SerializeField] private Sprite[] imagenesBocadillo = null;
    [SerializeField] private TypeWriter typeWriter;
    [SerializeField] private Animation animacion = null;
    [SerializeField] private GameObject bocadillo = null;


    [SerializeField] public AudioReverbZone reverberacion = null;

    public async void Start()
    {



        Application.targetFrameRate = 60;
        Application.runInBackground = true;

        

        ActivarDelegadosTypeWriter();    
        bocadillo.SetActive(false);
        DesactivarCanvas();
        ColorTextoCambio(notSelectedColor);

        canvasMenu[12].SetActive(true);
        canvasMenu[13].SetActive(false);
        reverberacion.enabled = false;

        //LOGO
        canvasMenu[4].SetActive(true);
        MusicManager.MusicInstance.PlayFXMenu(MusicManager.MusicInstance.sfx[13], false);
        canvasMenu[4].GetComponent<Animation>().Play("MenuActivate");

        MusicManager.MusicInstance.SettingsMixer(
           isMutedInternal,
           mainVolumenInternal,
           mainSoundInternal,
           mainSfxInternal
        );


        await UniTask.Delay(TimeSpan.FromSeconds(2));

        canvasMenu[4].GetComponent<Animation>().Play("MenuDesactivate");
        await UniTask.Delay(TimeSpan.FromSeconds(2));
        canvasMenu[4].SetActive(false);


        //ESPERAMOS 2 SEGUNDOS FONDO NEGRO
        canvasMenu[8].SetActive(true);
        await UniTask.Delay(TimeSpan.FromSeconds(2));


        //metodo 1---
        //{ 
        //    canvasAlphaMenu.alpha = 0;
        //    canvasMenu[4].SetActive(true);
        //    animations.Play("MenuActivate");
        //    MusicManager.MusicInstance.PlayInGameMusic( MusicManager.MusicInstance.sounds[0]);
        //    canvasMenu[4].SetActive(false);
        //    canvasMenu[0].SetActive(true);


        //}
        
        //metodo 2
        { 

            canvasMenu[12].GetComponent<ParallaxBackground>().scrollSpeed = 0.2f;
            reverberacion.enabled = false;

            canvasMenu[4].SetActive(true);
            animations.Play("MenuActivate");
            MusicManager.MusicInstance.PlayInGameMusic(MusicManager.MusicInstance.sounds[0], true, true);
            canvasMenu[4].SetActive(false);
            canvasMenu[12].SetActive(true);
            canvasMenu[8].SetActive(false);
            canvasMenu[0].SetActive(true);
            canvasMenu[13].SetActive(true);
            canvasMenu[11].SetActive(true);



        }
        


    }



    private void Awake()
    {

        GettingPlayerPrefsValues();


    }


    public async void ClickPlayGame()
    { 
    
        if (isBegun == true) return;
        isBegun = true;

        PlayFx(textos[0]);
        
        animations.Play("MenuDesactivate");
        await UniTask.Delay(TimeSpan.FromSeconds(2));

        canvasMenu[0].SetActive(false);
        canvasMenu[8].SetActive(true);
        canvasMenu[8].GetComponent<Animation>().Play("MenuDesactivate");
        await UniTask.Delay(TimeSpan.FromSeconds(2));
        canvasMenu[8].SetActive(false);
        //musica animacion
        //MusicManager.MusicInstance.PlayInGameMusic( MusicManager.MusicInstance.sounds[0]);
        MusicManager.MusicInstance.StopInGameMusic(); 



        if (debug == false)
        {
            // animacion texto

            reverberacion.enabled = false;


            MusicManager.MusicInstance.PlayInGameMusic2( MusicManager.MusicInstance.sounds[6], true, false );
            
            canvasMenu[9].SetActive(true);

            canvasMenu[11].SetActive(false);
            canvasMenu[12].SetActive(false);
            canvasMenu[14].SetActive(true);

            await UniTask.Delay(TimeSpan.FromSeconds(1));
            bocadillo.SetActive(true);

            imagenBocadillo.sprite = imagenesBocadillo[0];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[19];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[20];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[21];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            await UniTask.Delay(TimeSpan.FromSeconds(1));

            imagenBocadillo.sprite = imagenesBocadillo[0];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[19];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[20];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            imagenBocadillo.sprite = imagenesBocadillo[21];
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            await UniTask.Delay(TimeSpan.FromSeconds(1));

            
            for (ushort i = 0; i < 19; ++i)
            { 
            
                imagenBocadillo.sprite = imagenesBocadillo[i];
                await UniTask.Delay(TimeSpan.FromMilliseconds(150));

            }
             
            await UniTask.Delay(TimeSpan.FromMilliseconds(300));

            
            
            MusicManager.MusicInstance.PlayFXWhioutEffects(MusicManager.MusicInstance.sfx[17]);
            typeWriter.Write("<color=#11877b>—¡Hey! ¿Puedes ir a la tienda ahora?</color>");

            await UniTask.Delay(TimeSpan.FromSeconds(4));
            MusicManager.MusicInstance.StopWhioutEffects();
            await UniTask.Delay(TimeSpan.FromSeconds(4));
            
            MusicManager.MusicInstance.PlayFXWhioutEffects(MusicManager.MusicInstance.sfx[17]);
            typeWriter.Write("<color=#4670c2>¡Uff! Vaaaale mamá, está bien. Ya voy.</color>");
            
            await UniTask.Delay(TimeSpan.FromSeconds(4));
            MusicManager.MusicInstance.StopWhioutEffects();
            await UniTask.Delay(TimeSpan.FromSeconds(4));
            
           

            canvasMenu[6].SetActive(true);

            MusicManager.MusicInstance.StopPlayInGameMusic2();

            MusicManager.MusicInstance.PlayInGameMusic( MusicManager.MusicInstance.sounds[5], false, false );
            cinematicAnimation.Play("cinematica");

            await UniTask.Delay(TimeSpan.FromMilliseconds(1250));

            MusicManager.MusicInstance.PlayFXPasos(MusicManager.MusicInstance.sfx[16], true, false);

            float espera = cinematicAnimation.GetClip("cinematica").length
                + cinematicAnimation.GetClip("cinematica_fundidonegro").length;

            await UniTask.Delay(TimeSpan.FromSeconds(espera - 2f));

            MusicManager.MusicInstance.StopInGameMusic();
            MusicManager.MusicInstance.StopFXPasos();
            
            


            canvasMenu[9].SetActive(false);
        }


       
    }
   

    public async void ClickCreditos()
    { 


        PlayFx(textos[1]);
        canvasMenu[0].SetActive(false);
        canvasMenu[1].SetActive(true);




    }
   

    public async void ClickOpciones()
    { 
        PlayFx(textos[2]);
        DesactivarCanvas();


        if (isMutedInternal == true)
        {
            txtOptions[0].text = "ON";
        }
        else
        {

            txtOptions[0].text = "OFF";


        }

        txtOptions[1].text = mainVolumenInternal.ToString();
        txtOptions[2].text = mainSoundInternal.ToString();
        txtOptions[3].text = mainSfxInternal.ToString();

        canvasMenu[0].SetActive(false);
        canvasMenu[2].SetActive(true);
        canvasMenu[11].SetActive(true);
        canvasMenu[12].SetActive(true);
        canvasMenu[13].SetActive(true);


        
    
    }
   

    public async void ClickExit()
    { 
    
        MusicManager.MusicInstance.PlayFXMenu(   MusicManager.MusicInstance.sfx[1], false );


        //ShowPositionMenu(3);
        //ShowFX(positionTransformparticles[4].anchoredPosition);
        await UniTask.Delay(TimeSpan.FromMilliseconds(600));


        #if (UNITY_EDITOR)
            UnityEditor.EditorApplication.isPlaying = false;
        #elif (UNITY_STANDALONE) 
            Application.Quit();
        #elif (UNITY_WEBGL)
            Application.OpenURL("about:blank");
        #endif
  
    
    }


    private async void PlayFx(TextMeshProUGUI texto)
    { 
    
        MusicManager.MusicInstance.PlayFXMenu( MusicManager.MusicInstance.sfx[1], false  );


        texto.color = selectedColor;
        await UniTask.Delay(150);
        texto.color = notSelectedColor;

        await UniTask.Delay(200);
    
    }
   



    private void GettingPlayerPrefsValues()
    {


        if (PlayerPrefs.HasKey("isMutedInternal") == true)
        {

            int t = PlayerPrefs.GetInt("isMutedInternal");


            if (t == 0)
            {

                isMutedInternal = false;
            }
            else if (t == 1)
            {

                isMutedInternal = true;

            }



        }
        else
        {

            if (isMutedDefault == true)
            {

                PlayerPrefs.SetInt("isMutedInternal", 1);

            }
            else
            {

                PlayerPrefs.SetInt("isMutedInternal", 0);

            }


        }

        if (PlayerPrefs.HasKey("mainVolumenInternal") == true)
        {

            mainVolumenInternal = (ushort)PlayerPrefs.GetInt("mainVolumenInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenDefault);
        }


        if (PlayerPrefs.HasKey("mainSoundInternal") == true)
        {

            mainSoundInternal = (ushort)PlayerPrefs.GetInt("mainSoundInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainSoundInternal", mainSoundDefault);
        }



        if (PlayerPrefs.HasKey("mainSfxInternal") == true)
        {

            mainSfxInternal = (ushort)PlayerPrefs.GetInt("mainSfxInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainSfxInternal", mainSfxDefault);
        }



    }


    private void SavePlayerPrefsValues()
    {

        if (isMutedInternal == true)
        {

            PlayerPrefs.SetInt("isMutedInternal", 1);

        }
        else
        {

            PlayerPrefs.SetInt("isMutedInternal", 0);

        }

        PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenInternal);
        PlayerPrefs.SetInt("mainSfxInternal", mainSfxInternal);
        PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenInternal);

    }


    public void ResetPlayerPrefs()
    {

        PlayerPrefs.DeleteAll();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#elif UNITY_STANDALONE
        Application.Quit();
#endif


    }


    public void EnterHoverPlayGame()
    {


        if (isBegun == true) return;



        textos[0].color = selectedColor;
        
        ShowPunteroPosicion(0);


    }


    public void ExitHoverPlayGame()
    {
        if (isBegun == true) return;
        textos[0].color = notSelectedColor;

    }


    public void EnterHoverCreditos()
    {
        if (isBegun == true) return;
        textos[1].color = selectedColor;
        ShowPunteroPosicion(1);
    }


    public void ExitHoverCreditos()
    {
        if (isBegun == true) return;
        textos[1].color = notSelectedColor;

    }


    public void EnterHoverOpciones()
    {
        if (isBegun == true) return;
        textos[2].color = selectedColor;
        ShowPunteroPosicion(2);
    }


    public void ExitHoverOpciones()
    {
        if (isBegun == true) return;
        textos[2].color = notSelectedColor;

    }



    public void EnterHoverExit()
    {
        if (isBegun == true) return;
        textos[3].color = selectedColor;
        ShowPunteroPosicion(3);
    }


    public void ExitHoverExit()
    {
        if (isBegun == true) return;
        textos[3].color = notSelectedColor;

    }


    private void ShowPunteroPosicion(ushort contMenuPosition)
    {
        

         MusicManager.MusicInstance.PlayFXMenu(
        MusicManager.MusicInstance.sfx[0], false
        );

        var tempPos = positionpunteroMenuPrincipal[contMenuPosition].anchoredPosition;
        tempPos.x = punteroMenuPrincipal.anchoredPosition.x;
        punteroMenuPrincipal.anchoredPosition = tempPos;
        punteroMenuPrincipal.gameObject.SetActive(true);

    
    }

    public void ClickCredits()
    { 
    
        canvasMenu[1].SetActive(false);
        canvasMenu[0].SetActive(true);
    
    
    }

    public void EnterHoverCreditsAtras()
    { 
        textosOptions[6].color = selectedColor;
    
    }

    public void ExitHoverCreditsAtras()
    { 
        textosOptions[6].color = notSelectedColor;
    
    }



    #region OPCIONES


    public void MutedOnOff()
    { 
        print("muted" + isMutedInternal);
        isMutedInternal = !isMutedInternal;

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
        );

        MusicManager.MusicInstance.Mute();

        if (isMutedInternal == true)
        {
            txtOptions[0].text = "ON";
        }
        else
        {
            txtOptions[0].text = "OFF";
        }
    
    
    }

  


    public void IzqMainVolumen()
    { 

        print("izq main " + mainVolumenInternal);

        if (mainVolumenInternal > 0)
        {

            mainVolumenInternal -= 10;

            if (mainVolumenInternal <= 0)
            {

                mainVolumenInternal = 0;
            }

        }

        MusicManager.MusicInstance.SetVolumenMaster(mainVolumenInternal);
        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
        );

        txtOptions[1].text = mainVolumenInternal.ToString();
    
    }

    public void DerMainVolumnen()
    { 

        print("der main " + mainVolumenInternal);
    
        if (mainVolumenInternal < 100)
        {

            mainVolumenInternal += 10;

            if (mainVolumenInternal > 100)
            {

                mainVolumenInternal = 100;
            }

        }


        MusicManager.MusicInstance.SetVolumenMaster(mainVolumenInternal);
        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        txtOptions[1].text = mainVolumenInternal.ToString();

    }


    public void IzqSonidoVolumnen()
    { 
    
        print("izq sonido " + mainSoundInternal);

        if (mainSoundInternal > 0)
        {

            mainSoundInternal -= 10;

            if (mainSoundInternal <= 0)
            {

                mainSoundInternal = 0;
            }

        }

        MusicManager.MusicInstance.SetVolumenSounds(mainSoundInternal);

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        txtOptions[2].text = mainSoundInternal.ToString();
        
        
    }

    public void DerSonidoVolumen()
    { 

        print("der sonido " + mainSoundInternal);

        if (mainSoundInternal < 100)
        {

            mainSoundInternal += 10;

            if (mainSoundInternal > 100)
            {

                mainSoundInternal = 100;
            }

        }

        MusicManager.MusicInstance.SetVolumenSounds(mainSoundInternal);
        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        txtOptions[2].text = mainSoundInternal.ToString();
        
    
    }


    public void IzqSonidoFx()
    { 

        print("izq sonido fx " + mainSfxInternal);
        if (mainSfxInternal > 0)
        {

            mainSfxInternal -= 10;

            if (mainSfxInternal <= 0)
            {

                mainSfxInternal = 0;
            }

        }

        MusicManager.MusicInstance.SetVolumenSfx(mainSfxInternal);

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        txtOptions[3].text = mainSfxInternal.ToString();
    
    }

    public void DerSonidoFx()
    { 

        print("dersonido fx " + mainSfxInternal);
        if (mainSfxInternal < 100)
        {

            mainSfxInternal += 10;

            if (mainSfxInternal > 100)
            {

                mainSfxInternal = 100;
            }

        }

        MusicManager.MusicInstance.SetVolumenSfx(mainSfxInternal);

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        txtOptions[3].text = mainSfxInternal.ToString();
    
    }

    public async void BotonExitOptions()
    {

        SavePlayerPrefsValues();


        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );


        canvasMenu[0].SetActive(true);
        canvasMenu[2].SetActive(false);

    }









    public void EnterHoverMuted()
    {
        if (isBegun == true) return;

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );

        textosOptions[0].color = selectedColor;
        txtOptions[0].color = selectedColor;

    }


    public void ExitHoverMuted()
    {
        if (isBegun == true) return;
        textosOptions[0].color = notSelectedColor;
        txtOptions[0].color = notSelectedColor;
    }


    public void EnterHoverMainVolumen()
    {
        if (isBegun == true) return;
         MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );

        textosOptions[1].color = selectedColor;
        txtOptions[1].color = selectedColor;
    }


    public void ExitHoverMainVolumen()
    {
        if (isBegun == true) return;
        textosOptions[1].color = notSelectedColor;
        txtOptions[1].color = notSelectedColor;
    }


    
    public void EnterHoverMusicVolumen()
    {
        if (isBegun == true) return;

         MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );
        textosOptions[2].color = selectedColor;
        txtOptions[2].color = selectedColor;
    }


    public void ExitHoverMusicVolumen()
    {
        if (isBegun == true) return;
        textosOptions[2].color = notSelectedColor;
        txtOptions[2].color = notSelectedColor;
    }


    public void EnterHoverSFXVolumen()
    {
        if (isBegun == true) return;

         MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );
        textosOptions[3].color = selectedColor;
        txtOptions[3].color = selectedColor;
    }


    public void ExitHoverSFXVolumen()
    {
        if (isBegun == true) return;


        textosOptions[3].color = notSelectedColor;
        txtOptions[3].color = notSelectedColor;
    }

  

    public void ResetDefaultValues()
    {


        isMutedInternal = isMutedDefault;
        mainVolumenInternal = mainVolumenDefault;
        mainSoundInternal = mainSoundDefault;
        mainSfxInternal = mainSfxDefault;

        if (isMutedInternal == true)
        {
            txtOptions[0].text = "ON";
        }
        else
        {

            txtOptions[0].text = "OFF";
        }

        MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[1], false
            );

        
        MusicManager.MusicInstance.MuteOff();

        MusicManager.MusicInstance.SetVolumenMaster(mainVolumenInternal);
        MusicManager.MusicInstance.SetVolumenSounds(mainSoundInternal);
        MusicManager.MusicInstance.SetVolumenSfx(mainSfxInternal);

        txtOptions[1].text = mainVolumenInternal.ToString();
        txtOptions[2].text = mainSoundInternal.ToString();
        txtOptions[3].text = mainSfxInternal.ToString();

        
        



    }

    public void EnterHoverResetDefault()
    { 
         MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );
        textosOptions[4].color = selectedColor;

    }

    public void ExitHoverResetDefault()
    { 
    
        textosOptions[4].color = notSelectedColor;
    }

    public void EnterHoverAtras()
    { 

         MusicManager.MusicInstance.PlayFXMenu(
            MusicManager.MusicInstance.sfx[0], false
            );
        textosOptions[5].color = selectedColor;
    
    }

    public void ExitHoverAtras()
    { 
        textosOptions[5].color = notSelectedColor;
    
    }

    #endregion


    private void DesactivarCanvas()
    { 
    
        for(ushort i = 0; i < canvasMenu.Length; i++)
        { 
            canvasMenu[i].SetActive(false);
        
        }

        
    
    }

    private void ColorTextoCambio(Color colorToChange)
    { 
    
        for (ushort i = 0; i < textos.Length; i++)
        {
            textos[i].color = colorToChange;
        }
    
    }


    private void ActivarDelegadosTypeWriter()
    { 
        TextTokenizer.RegisterDefaultTokenizer();
    
    }

}
