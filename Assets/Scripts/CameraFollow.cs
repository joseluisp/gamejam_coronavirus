﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField] private Transform playerTransform = null;
    [SerializeField] private Transform cameraTransform = null;
    [SerializeField] private Camera camera = null;
    [SerializeField] private float offsetX = 2;
    [SerializeField] private float offsetY = 2;
    [SerializeField] private float dampTime = 10f;

    [SerializeField] private float margin = 0.1f;

    private void Awake()
    {
        if (playerTransform == null)
        { 
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        
        }

    }


    // Start is called before the first frame update
    void Start()
    {
        
    }


    private void Update()
    {
        
        if ((playerTransform is null) == false) 
        {
            
			float targetX = playerTransform.position.x + offsetX;
			float targetY = playerTransform.position.y + offsetY;

			if (Mathf.Abs(this.transform.position.x - targetX) > margin)
            { 
        	    targetX = Mathf.Lerp(this.transform.position.x, targetX, 1f/dampTime * Time.deltaTime);
            }
			

			if (Mathf.Abs(transform.position.y - targetY) > margin)
            { 
                targetY = Mathf.Lerp(this.transform.position.y, targetY, dampTime * Time.deltaTime);
            }
			
            
			this.transform.position = new Vector3(targetX, targetY, this.transform.position.z);
        }

    }

}
