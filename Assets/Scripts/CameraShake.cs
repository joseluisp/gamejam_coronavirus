﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraShake : MonoBehaviour
{
    [SerializeField] private GameManager gameManager = null;


    [SerializeField] public static float currentShake;
    [SerializeField] public float shakeDamp = 10;
    [SerializeField] public float shakeScale = 1;
    [SerializeField] public float shakeSpeed = 100.0f;
    [SerializeField] public Transform cameraTransform = null;
    
    private float offsetX = 0;
    private float offsetY = 0;
    public bool isCameraShaking = false;


    private void Awake()
    {
        offsetX = Random.Range(-1000.0f, 1000.0f);
        offsetY = Random.Range(-1000.0f, 1000.0f);
    }


    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void LateUpdate()
    {

        //print("emp=" + currentShake);

        if (isCameraShaking == true) return;

        if (currentShake > 0)
        { 
            isCameraShaking = true;
            currentShake = Dampen(currentShake, 0, shakeDamp, Time.deltaTime, 0.1f);

            var shakeStrength = currentShake * shakeScale;
            var shakeTime = Time.time * shakeSpeed;
            var localPosition = cameraTransform.localPosition;

            //localPosition = localPosition + Random.insideUnitSphere * shakeStrength;

            localPosition.x = Mathf.PerlinNoise(offsetX, shakeTime) * shakeStrength;
            localPosition.y = Mathf.PerlinNoise(offsetY, shakeTime) * shakeStrength;

            cameraTransform.localPosition = localPosition;
            isCameraShaking = false;
        
        }

       


    }


    private float Dampen(float current, float target, float damping, float elapsed, float minStep)
    {
        var factor = DampenFactor(damping, elapsed);
        var maxDelta = Mathf.Abs(target - current) * factor + minStep * elapsed;

        return Mathf.MoveTowards(current, target, maxDelta);
    }


    private float DampenFactor(float damping, float elapsed)
    {
        return 1f - Mathf.Pow((float)System.Math.E, -damping * elapsed);
    }




}

