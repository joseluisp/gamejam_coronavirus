﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UniRx.Async;
using System;

public enum EstadoActivador
{ 
    Activo,
    Desactivo

}

public class ActivadorController : MonoBehaviour
{

    [SerializeField] private Vector3 on = new Vector3(1, 0.38f, 1);
    [SerializeField] private Vector3 off = new Vector3(1, 0.28f, 1);
    [SerializeField] private OrugaController orugaController = null;
    [SerializeField] private TextMeshProUGUI txtExplicacion = null;
    [SerializeField] private bool activadorPorPeso = false;
    [SerializeField] private string mensaje = "Activar Pulsa Q o E";
    [SerializeField] private EstadoActivador estadoActivador = EstadoActivador.Desactivo;
    [SerializeField] private GameObject player = null;

    private bool isPlayerEntered = false;


    private void OnEnable()
    {
        this.transform.localScale = on;
        if (player == null)
        { 
            player = GameObject.FindGameObjectWithTag("Player");
        
        }    
    }

    private void Awake()
    {
        this.transform.localScale = on;
      

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (activadorPorPeso == true)
        { 
            if (collision.CompareTag("Player") || collision.CompareTag("Objetos"))
            { 
                //frenamos al jugador.
                collision.attachedRigidbody.velocity = Vector2.zero;
                isPlayerEntered = true;
                this.GetComponent<BoxCollider2D>().enabled = false;
                RealizarActivacion(collision.gameObject.transform);

            }
        
        }
        else
        { 
            if (collision.CompareTag("Player"))
            { 
                //frenamos al jugador.
                collision.attachedRigidbody.velocity = Vector2.zero;
                isPlayerEntered = true;
                txtExplicacion.text = mensaje;
                collision.GetComponent<PlayerMovement>().EntradaActivador();
                

            }
        
        
        }


       




    }


    private void Update()
    {

        if (activadorPorPeso == true) return;

        if (isPlayerEntered == true)
        { 

            print(player.GetComponent<PlayerMovement>().realizarActivacion);

            if (estadoActivador == EstadoActivador.Desactivo)
            {
                if (player.GetComponent<PlayerMovement>().realizarActivacion == true)
                { 

                    print(estadoActivador);
                    print("activar");
                    estadoActivador = EstadoActivador.Activo;
                    RealizarActivacion(player.transform);
                    return;
                
                }
            }

            
            if (estadoActivador == EstadoActivador.Activo)
            {
                if (player.GetComponent<PlayerMovement>().realizarDesActivacion == true)
                { 
                    print("desactivar");
                    estadoActivador = EstadoActivador.Desactivo;
                    RealizarDesActivacion(player.transform);
                    return;

                }  


            }
        
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        { 
            txtExplicacion.text = "";
            isPlayerEntered = false;
            collision.GetComponent<PlayerMovement>().SalirActivador();
        }

    }


    private void RealizarActivacion(Transform objeto)
    { 
    
        //elevamos algo al player para darle efecto
        objeto.position = new Vector3(
            objeto.position.x,
            objeto.position.y + 0.2f,
            objeto.position.z
                
            );
            
        objeto.position = new Vector3(
            objeto.position.x,
            objeto.position.y - 0.2f,
            objeto.position.z
                
            );
            


        this.transform.localScale = off;
        orugaController.ActivarOruga();
    
    
    
    }


    private void RealizarDesActivacion(Transform objeto)
    { 
    
        //elevamos algo al player para darle efecto
        objeto.position = new Vector3(
            objeto.position.x,
            objeto.position.y + 0.2f,
            objeto.position.z
                
            );
            
        this.transform.localScale = on;
        orugaController.DesactivarOruga();
    
    
    
    }

}
