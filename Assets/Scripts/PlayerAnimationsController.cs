﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationsController : MonoBehaviour
{
    [SerializeField] public Animator animator = null;

    public enum UnitStay
    {
        stand, fly, sitdown
    };

    public UnitStay unitStay;

    public enum UnitTrigger
    {
        Idle_Trigger, Fly_Trigger, SitDown_Trigger, JumpUp_Trigger, JumpDown_Trigger, DashFront_Trigger, DashBack_Trigger,
        Tackle_Trigger, EyeOpen_Trigger, EyeClose_Trigger, KnockBackF_Trigger, KnockBackB_Trigger, AttackUp_Trigger, AttackDown_Trigger, 
        Throw_Trigger, GunShoot_Trigger, Sting_Trigger, DamageFront_Trigger, DamageBack_Trigger, Cast_Trigger
    };
    public UnitTrigger unitTrigger;

    [Header("Animation bool")]
    public bool m_moving = false;
    public bool m_dash = false;
    public bool m_knockback = false;
    public bool m_sting = false;

    [Header("Animations Triggers")]
    public string trigger_Idle = "trigger_Idle";
    public string trigger_Fly = "trigger_Fly";
    public string trigger_SitDown = "trigger_SitDown";
    public string trigger_JumpUp = "trigger_JumpUp";
    public string trigger_dash_Front = "trigger_dash_Front";
    public string triggger_dash_Back = "triggger_dash_Back";
    public string trigger_attackUp = "trigger_attackUp";
    public string trigger_attackDown = "trigger_attackDown";
    public string trigger_knowckBack_Front = "trigger_knowckBack_Front";
    public string trigger_knowckBack_Back = "trigger_knowckBack_Back";
    public string trigger_damageFront = "trigger_damageFront";
    public string trigger_damangeBack = "trigger_damangeBack";


    public float unityVelicityX = 0f;
    public float unityVelicityY = 0f;


    public void Set_Move()
    { 
        if (animator.GetBool("move") == false)
        { 
            animator.SetBool("move", true);
            animator.SetBool("falling", false);
        }
        animator.SetBool("falling", false);
    }

    public void Set_Idle()
    { 
        animator.SetBool("move", false);
        animator.SetBool("falling", false);
    }

    public void Set_Dash()
    { 
    
        animator.SetBool("Dash", true);
    
    }
    public void SetNot_Dash()
    {

        animator.SetBool("Dash", false);

    }
    public void Set_Slide()
    {
        if(!animator.GetBool("Slide"))
        {
            animator.SetBool("Slide", true);
        }
        animator.SetBool("Dash", false);
    }
    public void Set_Jump()
    {
      animator.SetBool("move", false); 
      animator.SetBool("jump", true);
        
    }
    public void SetFalling()
    {
        animator.SetBool("jump", false);
        animator.SetBool("falling", true);
    }
    public void SetNotSliding()
    {
        if (animator.GetBool("Slide"))
        {
            animator.SetBool("Slide", false);
        }
    }
    public void SetWallJump()
    {
        animator.SetBool("WallJump", true);
    }
    public void SetNotWallJump()
    {
        animator.SetBool("WallJump", false);
    }
    public void Set_UnitStay()
    { 
    
    
    
    }


    public void UnitSting(bool val)
    {
        Set_UnitStay();
        animator.SetBool("Sting", val);
    }

    public void UnitSetVelocityX(float inX)
    {
        animator.SetFloat("VelocityX",inX);
    }
 
    
    public void UnitSetTrigger(UnitTrigger tri)
    {
        switch (tri)
        {

            case UnitTrigger.Idle_Trigger:
                animator.SetTrigger(trigger_Idle);
                break;

            case UnitTrigger.Fly_Trigger:
                animator.SetTrigger(trigger_Fly);
                break;

            case UnitTrigger.SitDown_Trigger:
                animator.SetTrigger(trigger_SitDown);
                break;

            case UnitTrigger.JumpUp_Trigger:
                animator.SetTrigger(trigger_JumpUp);
                break;
        }

    }


}