﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using System;


public class MovementKid : MonoBehaviour
{

    [SerializeField] private SpriteRenderer render = null;
    [SerializeField] private Sprite[] listadoIdleImagesKid = null;
    [SerializeField] private Sprite[] listadoAbajoImagesKid = null;
    [SerializeField] private Sprite[] listadoIzquierdaImagesKid = null;
    [SerializeField] private Sprite[] listadoSorpresaImagesKid = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitKid()
    { 
    
    
    
    }

    public async void Idle()
    { 
        
        render.sprite = listadoIdleImagesKid[0];
        await UniTask.Delay(300);
        render.sprite = listadoIdleImagesKid[1];
        await UniTask.Delay(300);
        render.sprite = listadoIdleImagesKid[2];
        await UniTask.Delay(300);
        render.sprite = listadoIdleImagesKid[3];
        await UniTask.Delay(300);
    
    }

    public async void Abajo()
    { 
    
        //render.sprite = listadoAbajoImagesKid[0];
        //await UniTask.Delay(300);
        //render.sprite = listadoAbajoImagesKid[1];
        //await UniTask.Delay(300);
        //render.sprite = listadoAbajoImagesKid[2];
        //await UniTask.Delay(300);
        //render.sprite = listadoAbajoImagesKid[3];
        //await UniTask.Delay(300);
    
    }


    public async void Arriba()
    { 
    
    
    }

    public async void Izquierda()
    {
        for (ushort i = 0; i < 8; ++i)
        { 
            render.sprite = listadoIzquierdaImagesKid[0];
            await UniTask.Delay(150);
            render.sprite = listadoIzquierdaImagesKid[1];
            await UniTask.Delay(150);
            render.sprite = listadoIzquierdaImagesKid[2];
            await UniTask.Delay(150);
            render.sprite = listadoIzquierdaImagesKid[3];
            await UniTask.Delay(150);
            render.sprite = listadoIzquierdaImagesKid[4];
            await UniTask.Delay(150);
            render.sprite = listadoIzquierdaImagesKid[5];
            await UniTask.Delay(150);


        
        }

       

        

    }


    public async void  Alerta()
    {
        for (ushort i = 0; i < 6; ++i)
        { 
        
            render.sprite = listadoSorpresaImagesKid[0];
            await UniTask.Delay(300);
            render.sprite = listadoSorpresaImagesKid[1];
            await UniTask.Delay(300);
            render.sprite = listadoSorpresaImagesKid[2];
            await UniTask.Delay(300);
            render.sprite = listadoSorpresaImagesKid[3];
            await UniTask.Delay(300);

        
        } 
    
    
    }

    public async void Derecha()
    { 
    
    
    
    }


}
