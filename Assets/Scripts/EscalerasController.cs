﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscalerasController : MonoBehaviour
{

    [SerializeField] private BoxCollider2D ultimoEscalon = null;
    public bool isArriba = false;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            collision.GetComponent<PlayerMovement>().Set_ClimbStairs();

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            collision.GetComponent<PlayerMovement>().Set_ClimbStairs();

        }


    }


    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<PlayerMovement>().Set_ExitStairs();
            

        }

    }



}
