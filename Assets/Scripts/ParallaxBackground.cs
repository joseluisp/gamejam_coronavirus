﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{


    [SerializeField] public GameObject[] levels;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Vector2 screenBounds;
    [SerializeField] public float choke;
    [SerializeField] public float scrollSpeed;
    [SerializeField] private bool moverPantallaMenu = false;

    private Vector3 lastScreenPosition;

    void Start()
    {
        if (mainCamera == null)
        { 
            mainCamera = this.gameObject.GetComponent<Camera>();
        
        }


        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));

        for (ushort i = 0; i < levels.Length; i++)
        { 
            loadChildObjects(levels[i]);
        
        }

        lastScreenPosition = this.transform.position;


    }



    private void loadChildObjects(GameObject obj)
    {

        float objectWidth = obj.GetComponent<SpriteRenderer>().bounds.size.x - choke;
        int childsNeeded = (int)Mathf.Ceil(screenBounds.x * 2 / objectWidth);
        GameObject clone = GameObject.Instantiate(obj);
        
        for(ushort i = 0; i <= childsNeeded; i++)
        {
            GameObject c = Instantiate(clone) as GameObject;
            c.transform.SetParent(obj.transform);
            c.transform.position = new Vector3(objectWidth * i, obj.transform.position.y, obj.transform.position.z);
            c.name = obj.name + i;

        }
        Destroy(clone);
        Destroy(obj.GetComponent<SpriteRenderer>());


    }
    void repositionChildObjects(GameObject obj)
    {

        Transform[] children = obj.GetComponentsInChildren<Transform>();
        if(children.Length > 1)
        {

            GameObject firstChild = children[1].gameObject;
            GameObject lastChild = children[children.Length - 1].gameObject;
            float halfObjectWidth = lastChild.GetComponent<SpriteRenderer>().bounds.extents.x - choke;
            if(this.transform.position.x + screenBounds.x > lastChild.transform.position.x + halfObjectWidth)
            {
                firstChild.transform.SetAsLastSibling();
                firstChild.transform.position = new Vector3(lastChild.transform.position.x + halfObjectWidth * 2,
                    lastChild.transform.position.y,
                    lastChild.transform.position.z);
            }
            else if(this.transform.position.x - screenBounds.x < firstChild.transform.position.x - halfObjectWidth)
            {
                lastChild.transform.SetAsFirstSibling();
                lastChild.transform.position = new Vector3(firstChild.transform.position.x - halfObjectWidth * 2,
                    firstChild.transform.position.y,
                    firstChild.transform.position.z);
            }
        }
    }
    void Update()
    {

        if (moverPantallaMenu == true)
        { 
            Vector3 velocity = Vector3.zero;
            Vector3 desiredPosition = transform.position + new Vector3(scrollSpeed, 0, 0);
            Vector3 smoothPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, 0.3f);
            this.transform.position = smoothPosition;
        
        }

        

    }

    private void LateUpdate()
    {
        for (ushort i = 0; i < levels.Length; i++)
        {
            repositionChildObjects(levels[i]); 
            float parallaxSpeed = 1 - Mathf.Clamp01(Mathf.Abs(this.transform.position.z / levels[i].transform.position.z));
            //print("parallaxspeed" + parallaxSpeed);
            float difference = this.transform.position.x - lastScreenPosition.x;
            levels[i].transform.Translate(Vector3.right * difference * parallaxSpeed );
            

        }

        lastScreenPosition = this.transform.position;
    }
}