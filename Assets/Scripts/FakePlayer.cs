﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using System;
using UnityEngine.EventSystems;

public class FakePlayer : MonoBehaviour
{

    [SerializeField] private float runSpeed = 40f;
    [SerializeField] private float verticalSpeed = 10f;
    [SerializeField] private ParticleSystem particula = null;
    [SerializeField] private Rigidbody2D rigid = null;
    [Range(0, 0.3f)] [SerializeField] private float m_MovementSmoothing = 0.05f;	

    [SerializeField] private Animator animator = null;
    [SerializeField] private PlayerController playerController = null;
    [SerializeField] private PlayerMovement playerMovement = null;
    private Vector3 m_Velocity = Vector3.zero;
    [SerializeField] private float speedMovement = 1f;

    [SerializeField] private PlayerAnimationsController animationsController = null;

    // Start is called before the first frame update
    private async void Start()
    {
        startPos = transform.position;
        await UniTask.Delay(TimeSpan.FromSeconds(2));
        canMove = true;
        

    }


    public void ShowParticlesMove()
    { 
        animationsController.Set_Move();
        var tempRot = particula.transform.rotation;
        tempRot.eulerAngles = new Vector3(0, 0, 135f);
        particula.transform.rotation = tempRot;
        particula.Play();
    
    }

    public void SetJumping()
    { 
        
        animationsController.Set_Jump();
    
    }


    private bool canMove = false;
    private bool isJumping = false;
    private void FixedUpdate()
    {
        if (canMove == true)
        { 
            rigid.drag = 0f;
            Vector3 targetVelocity = new Vector2(speedMovement, rigid.velocity.y);
            rigid.velocity = Vector3.SmoothDamp(rigid.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            playerMovement.ShowParticlesMove();
        
        }

        if (isJumping == true)
        { 



            //print("jumping");
            //canMove = false;
            //rigid.velocity = Vector2.up * 15f; //15
            //rigid.velocity = new Vector2(50f, rigid.velocity.y); //%
            //isJumping = false;
            //canMove = true;
            //animationsController.Set_Move();
        }

        

    }

 //   [Tooltip("Position we want to hit")]
	//public Transform targetPos;
	
	//[Tooltip("Horizontal speed, in units/sec")]
	//public float speed = 10;
	
	//[Tooltip("How high the arc should be, in units")]
	//public float arcHeight = 1;
	
	
	Vector3 startPos;
	
    private bool shot = false;
		
	void Update() 
    {

  //      if (shot == false) return;
  //      shot = false;

		//// Compute the next position, with arc added in
		//float x0 = startPos.x;
		//float x1 = targetPos.position.x;
		//float dist = x1 - x0;
		//float nextX = Mathf.MoveTowards(transform.position.x, x1, speed * Time.deltaTime);
		//float baseY = Mathf.Lerp(startPos.y, targetPos.position.y, (nextX - x0) / dist);
		//float arc = arcHeight * (nextX - x0) * (nextX - x1) / (-0.25f * dist * dist);
		//var nextPos = new Vector3(nextX, baseY + arc, transform.position.z);
		
		//// Rotate to face the next position, and then move there
		//transform.rotation = LookAt2D(nextPos - transform.position);
		//transform.position = nextPos;
		
		//// Do something when we reach the target
		//if (nextPos == targetPos.position) Arrived();
        

  //      animationsController.Set_Move();

	}
	
	
	/// 
	/// This is a 2D version of Quaternion.LookAt; it returns a quaternion
	/// that makes the local +X axis point in the given forward direction.
	/// 
	/// forward direction
	/// Quaternion that rotates +X to align with forward
	//static Quaternion LookAt2D(Vector2 forward) {
	//	return Quaternion.Euler(0, 0, Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg);
	//}



    bool jumped = false;

    private async void OnTriggerEnter2D(Collider2D collision)
    {

        //if (collision.CompareTag("Danger"))
        //{ 

        //    if (jumped == true) return;
        //    jumped = true;
        //    shot = true;
        //    print("salto");
        //    isJumping = true;
        //    playerMovement.SetJumping();
        //    await UniTask.Delay(TimeSpan.FromSeconds(4));
        //    jumped = false;
            
        //    return;

        //}

        //if (collision.CompareTag("Fuera"))
        //{ 
        //    this.gameObject.transform.position = new Vector3(
        //        this.gameObject.transform.position.x + 5,
        //        this.gameObject.transform.position.y + 5,
        //        this.gameObject.transform.position.z
                
        //        );
        


        //}


        
    }

   



    private bool jumping = false;

    







}
