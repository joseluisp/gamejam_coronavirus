﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;

public class UltimoEscalon : MonoBehaviour
{

    [SerializeField] private EscalerasController escaleras = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }


    private async void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            escaleras.isArriba = true;
            
            await UniTask.Delay(500);
            escaleras.GetComponent<BoxCollider2D>().enabled = false;

        }
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
            escaleras.isArriba = false;
            escaleras.GetComponent<BoxCollider2D>().enabled = true;

        }


    }



   
}
