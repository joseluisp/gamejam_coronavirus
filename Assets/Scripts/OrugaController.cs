﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OrugaController : MonoBehaviour
{

    [SerializeField] private SurfaceEffector2D superficie = null;
    [SerializeField] private bool orugaActivaPrincipio = true;

    public TextMeshProUGUI texto = null;

    private void Awake()
    {

        if (superficie is null)
        { 
            superficie = this.GetComponent<SurfaceEffector2D>();
        
        }

        superficie.enabled = orugaActivaPrincipio;

    }


    //private void Update()
    //{


    //    texto.text = superficie.enabled.ToString();
    //}



    public void ActivarOruga()
    {

        superficie.enabled = true;

    }

    public void DesactivarOruga()
    { 
    
        superficie.enabled = false;
    
    }


}
