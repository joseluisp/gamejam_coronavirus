using UnityEngine;
using System.Collections.Generic;

namespace IgniteModule.TypeWrite
{
    [CreateAssetMenu(fileName = "TypeWriterSettings", menuName = "IgniteModule/TypeWriterSettings")]
    public class TypeWriterSettings : ScriptableObject
    {
        [SerializeField] float defaultDelaySeconds;
        [SerializeField] float punctuationDelaySeconds;
        [SerializeField] List<char> punctuationMarks;

        WaitForSeconds defaultDelay;
        WaitForSeconds punctuationDelay;

        /// <summary> デフォルトのディレイ </summary>
        public WaitForSeconds DefaultDelay => defaultDelay != null ? defaultDelay : (defaultDelay = new WaitForSeconds(defaultDelaySeconds));
        /// <summary> 句読点後のディレイ </summary>
        public WaitForSeconds PunctuationDelay => punctuationDelay != null ? punctuationDelay : (punctuationDelay = new WaitForSeconds(punctuationDelaySeconds));
        /// <summary> 句読点として扱う文字 </summary>
        public IReadOnlyList<char> PunctuationMarks => punctuationMarks;
    }
}