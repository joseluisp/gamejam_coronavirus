using System.Collections;
using System.Threading;

namespace IgniteModule.TypeWrite
{
    public interface IToken
    {
        string Text { get; }

        void Print(IWriter writer);

        IEnumerator PrintCoroutine(IWriter writer, TypeWriterCancel cancel);
    }
}