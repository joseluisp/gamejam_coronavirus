namespace IgniteModule.TypeWrite
{
    public interface ITokenizer
    {
        IToken Tokenize(string rawToken);
    }
}